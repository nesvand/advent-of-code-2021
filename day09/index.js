import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

export function answerA(lines) {
    const grid = lines

    let totalRisk = 0
    for (let y = 0; y < grid.length; y++) {
        const row = grid[y]
        for (let x = 0; x < row.length; x++) {
            const current = row[x]

            if (
                (!(y - 1 >= 0) || current < grid[y - 1][x]) &&
                (!(y + 1 < grid.length) || current < grid[y + 1][x]) &&
                (!(x - 1 >= 0) || current < grid[y][x - 1]) &&
                (!(x + 1 < row.length) || current < grid[y][x + 1])
            ) {
                totalRisk += Number(current) + 1
            }
        }
    }

    return totalRisk
}

function floodFill(x, y, map) {
    // console.trace()
    if (map[y][x] === 'x' || map[y][x] === '*') return 0
    map[y][x] = 'x'

    let size = 1

    if (y - 1 >= 0) size += floodFill(x, y - 1, map)
    if (y + 1 < map.length) size += floodFill(x, y + 1, map)
    if (x - 1 >= 0) size += floodFill(x - 1, y, map)
    if (x + 1 < map[0].length) size += floodFill(x + 1, y, map)

    return size
}

export function answerB(lines) {
    const grid = lines
    const map = Array(grid.length)
        .fill(0)
        .map((_, y) =>
            Array(grid[0].length)
                .fill(0)
                .map((_, x) => (grid[y][x] === '9' ? '*' : '.')),
        )

    // map.forEach((row) => console.log(row.join('')))

    const basins = []

    for (let y = 0; y < grid.length; y++) {
        const row = grid[y]
        for (let x = 0; x < row.length; x++) {
            const size = floodFill(x, y, map)

            if (size > 0) {
                basins.push(size)
            }
        }
    }
    basins.sort((a, b) => b - a)
    return basins[0] * basins[1] * basins[2]
}
