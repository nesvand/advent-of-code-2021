import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

export function answerA(lines) {
    const positions = lines[0].split(',').map(Number)

    const maxPos = positions.reduce((max, p) => Math.max(max, p), Number.NEGATIVE_INFINITY)
    const minPos = positions.reduce((min, p) => Math.min(min, p), Number.POSITIVE_INFINITY)

    const minimisedFuelCost = {
        finalPos: null,
        fuelUsed: Number.POSITIVE_INFINITY,
    }
    for (let i = minPos; i <= maxPos; i++) {
        const fuelUsed = positions.reduce((total, pos) => total + Math.abs(pos - i), 0)

        if (fuelUsed < minimisedFuelCost.fuelUsed) {
            minimisedFuelCost.finalPos = i
            minimisedFuelCost.fuelUsed = fuelUsed
        }
    }

    return minimisedFuelCost.fuelUsed
}

export function answerB(lines) {
    const positions = lines[0].split(',').map(Number)

    const maxPos = positions.reduce((max, p) => Math.max(max, p), Number.NEGATIVE_INFINITY)
    const minPos = positions.reduce((min, p) => Math.min(min, p), Number.POSITIVE_INFINITY)

    const minimisedFuelCost = {
        finalPos: null,
        fuelUsed: Number.POSITIVE_INFINITY,
    }
    for (let i = minPos; i <= maxPos; i++) {
        const fuelUsed = positions.reduce((total, pos) => {
            const n = Math.abs(pos - i)
            const nthTerm = (n * (n + 1)) / 2
            return total + nthTerm
        }, 0)

        if (fuelUsed < minimisedFuelCost.fuelUsed) {
            minimisedFuelCost.finalPos = i
            minimisedFuelCost.fuelUsed = fuelUsed
        }
    }

    return minimisedFuelCost.fuelUsed
}
