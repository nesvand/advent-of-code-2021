import { readData, answerA, answerB } from './index.js'

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: readData('./test'),
            result: 5934,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 388739,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'test data',
            data: readData('./test'),
            result: 26984457539,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 1741362314973,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
