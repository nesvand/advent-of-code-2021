import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

const decrementAge = (group) => group.age--

export function answerA(lines, days = 80) {
    const babyGroups = []
    const adultGroups = lines[0].split(',').reduce((adults, input) => {
        const inputAge = parseInt(input, 10)
        const group = adults.find(({ age }) => age === inputAge)

        if (group) {
            group.count++
        } else {
            adults.push({ count: 1, age: inputAge })
        }

        return adults
    }, [])

    let day = 0
    while (day < days) {
        const adultBirtherIndex = adultGroups.indexOf(adultGroups.find(({ age }) => age === 0))
        let [adultBirthingGroup] = adultBirtherIndex >= 0 ? adultGroups.splice(adultBirtherIndex, 1) : []
        const babyBirtherIndex = babyGroups.indexOf(babyGroups.find(({ age }) => age === 0))
        let [babyBirthingGroup] = babyBirtherIndex >= 0 ? babyGroups.splice(babyBirtherIndex, 1) : []

        babyGroups.forEach(decrementAge)
        adultGroups.forEach(decrementAge)

        const birtherCount = (adultBirthingGroup?.count ?? 0) + (babyBirthingGroup?.count ?? 0)
        if (birtherCount) {
            babyGroups.push({ count: birtherCount, age: 8 })
            adultGroups.push({ count: birtherCount, age: 6 })
        }

        day++
    }

    return (
        adultGroups.reduce((count, group) => count + group.count, 0) +
        babyGroups.reduce((count, group) => count + group.count, 0)
    )
}

export function answerB(lines) {
    return answerA(lines, 256)
}
