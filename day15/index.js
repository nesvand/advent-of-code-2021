import { readFileSync } from 'fs'
import { resolve } from 'path'
import { Graph, aStarSearch } from '../utils/astar'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

export function answerA(lines) {
    const map = lines.map((line) => line.split``.filter(Boolean).map(Number))

    const graph = new Graph(map)
    const start = graph.grid[0][0]
    const end = graph.grid[map[0].length - 1][map.length - 1]
    const result = aStarSearch(graph, start, end)

    return result.map((node) => node.weight).reduce((a, b) => a + b)
}

const copySegmentWithOffset = (segment, offsetx, offsety) =>
    Array.from({ length: segment.length }, (_, y) =>
        Array.from({ length: segment[0].length }, (__, x) => ((segment[y][x] - 1 + offsetx + offsety) % 9) + 1),
    )

export function answerB(lines) {
    const mapSegment = lines.map((line) => line.split``.filter(Boolean).map(Number))
    const fullMap = Array.from({ length: 5 }, (_, y) =>
        Array.from({ length: 5 }, (__, x) => copySegmentWithOffset(mapSegment, x, y)),
    )
        .map((megaRow) => {
            const row = Array.from({ length: megaRow[0].length }, () => [])
            for (const map of megaRow) {
                for (let y = 0; y < map.length; y++) {
                    row[y].push(...map[y])
                }
            }
            return row
        })
        .reduce((final, megaRow) => (final.push(...megaRow), final), [])

    const graph = new Graph(fullMap)
    const start = graph.grid[0][0]
    const end = graph.grid[fullMap[0].length - 1][fullMap.length - 1]
    const result = aStarSearch(graph, start, end)

    return result.map((node) => node.weight).reduce((a, b) => a + b)
}
