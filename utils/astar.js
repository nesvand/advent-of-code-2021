/*
 * Copyright (c) 2021 Andrew Nesvadba
 * Copyright (c) Brian Grinstead, http://briangrinstead.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

export class Graph {
    /**
     * @param {number[][]} gridIn
     **/
    constructor(gridIn) {
        this.nodes = []
        this.grid = []
        this.dirtyNodes = []

        for (let x = 0; x < gridIn.length; x++) {
            this.grid[x] = []

            for (let y = 0, row = gridIn[x]; y < row.length; y++) {
                const node = new GridNode(x, y, row[y])
                this.grid[x][y] = node
                this.nodes.push(node.clean())
            }
        }
    }

    /**
     * @param {GridNode} node
     **/
    markDirty(node) {
        this.dirtyNodes.push(node)
    }

    /**
     * @param {GridNode} node
     **/
    neighbours(node) {
        const ret = []
        const { x, y } = node
        const grid = this.grid

        // West
        if (grid[x - 1] && grid[x - 1][y]) {
            ret.push(grid[x - 1][y])
        }

        // East
        if (grid[x + 1] && grid[x + 1][y]) {
            ret.push(grid[x + 1][y])
        }

        // South
        if (grid[x] && grid[x][y - 1]) {
            ret.push(grid[x][y - 1])
        }

        // North
        if (grid[x] && grid[x][y + 1]) {
            ret.push(grid[x][y + 1])
        }

        return ret
    }
}

class GridNode {
    /**
     * @param {number} x
     * @param {number} y
     * @param {number} weight
     **/
    constructor(x, y, weight) {
        this.x = x
        this.y = y
        this.weight = weight
    }

    /**
     * @param {GridNode} fromNeighbor
     **/
    getCost(fromNeighbor) {
        // Take diagonal weight into consideration.
        if (fromNeighbor && fromNeighbor.x != this.x && fromNeighbor.y != this.y) {
            return this.weight * 1.41421
        }
        return this.weight
    }

    isWall() {
        return this.weight === 0
    }

    clean() {
        this.f = 0
        this.g = 0
        this.h = 0
        this.visited = false
        this.closed = false
        this.parent = null

        return this
    }
}

class BinaryHeap {
    /**
     * @param {function(GridNode): number} scoreFunction
     **/
    constructor(scoreFunction) {
        this.content = []
        this.scoreFunction = scoreFunction
    }

    /**
     * @param {GridNode} element
     **/
    push(element) {
        // Add the new element to the end of the array.
        this.content.push(element)

        // Allow it to sink down.
        this.sinkDown(this.content.length - 1)
    }

    pop() {
        // Store the first element so we can return it later.
        const result = this.content[0]
        // Get the element at the end of the array.
        const end = this.content.pop()
        // If there are any elements left, put the end element at the
        // start, and let it bubble up.
        if (this.content.length > 0) {
            this.content[0] = end
            this.bubbleUp(0)
        }
        return result
    }

    remove(node) {
        const i = this.content.indexOf(node)

        // When it is found, the process seen in 'pop' is repeated
        // to fill up the hole.
        const end = this.content.pop()

        if (i !== this.content.length - 1) {
            this.content[i] = end

            if (this.scoreFunction(end) < this.scoreFunction(node)) {
                this.sinkDown(i)
            } else {
                this.bubbleUp(i)
            }
        }
    }

    size() {
        return this.content.length
    }

    rescoreElement(node) {
        this.sinkDown(this.content.indexOf(node))
    }

    sinkDown(n) {
        // Fetch the element that has to be sunk.
        const element = this.content[n]

        // When at 0, an element can not sink any further.
        while (n > 0) {
            // Compute the parent element's index, and fetch it.
            const parentN = ((n + 1) >> 1) - 1
            const parent = this.content[parentN]
            // Swap the elements if the parent is greater.
            if (this.scoreFunction(element) < this.scoreFunction(parent)) {
                this.content[parentN] = element
                this.content[n] = parent
                // Update 'n' to continue at the new position.
                n = parentN
            }

            // Found a parent that is less, no need to sink any further.
            else {
                break
            }
        }
    }

    bubbleUp(n) {
        // Look up the target element and its score.
        const length = this.content.length
        const element = this.content[n]
        const elemScore = this.scoreFunction(element)

        while (true) {
            // Compute the indices of the child elements.
            const child2N = (n + 1) << 1
            const child1N = child2N - 1
            // This is used to store the new position of the element, if any.
            let swap = null
            let child1Score
            // If the first child exists (is inside the array)...
            if (child1N < length) {
                // Look it up and compute its score.
                var child1 = this.content[child1N]
                child1Score = this.scoreFunction(child1)

                // If the score is less than our element's, we need to swap.
                if (child1Score < elemScore) {
                    swap = child1N
                }
            }

            // Do the same checks for the other child.
            if (child2N < length) {
                const child2 = this.content[child2N]
                const child2Score = this.scoreFunction(child2)
                if (child2Score < (swap === null ? elemScore : child1Score)) {
                    swap = child2N
                }
            }

            // If the element needs to be moved, swap it, and continue.
            if (swap !== null) {
                this.content[n] = this.content[swap]
                this.content[swap] = element
                n = swap
            }

            // Otherwise, we are done.
            else {
                break
            }
        }
    }
}

function heuristic(pos0, pos1) {
    const d1 = Math.abs(pos1.x - pos0.x)
    const d2 = Math.abs(pos1.y - pos0.y)
    return d1 + d2
}

/**
 * @param {Graph} graph
 * @param {GraphNode} start
 * @param {GraphNode} end
 **/
export function aStarSearch(graph, start, end) {
    const openHeap = new BinaryHeap((node) => node.f)

    start.h = heuristic
    graph.markDirty(start)
    openHeap.push(start)

    while (openHeap.size() > 0) {
        const currentNode = openHeap.pop()

        if (currentNode === end) {
            const path = []
            let curr = currentNode
            while (curr.parent) {
                path.unshift(curr)
                curr = curr.parent
            }
            return path
        }

        currentNode.closed = true

        const neighbours = graph.neighbours(currentNode)

        for (const neighbour of neighbours) {
            if (neighbour.closed || neighbour.isWall()) {
                continue
            }

            const gScore = currentNode.g + neighbour.getCost(currentNode)
            const beenVisited = neighbour.visited

            if (!beenVisited || gScore < neighbour.g) {
                neighbour.visited = true
                neighbour.parent = currentNode
                neighbour.h = neighbour.h || heuristic(neighbour, end)
                neighbour.g = gScore
                neighbour.f = neighbour.g + neighbour.h
                graph.markDirty(neighbour)

                if (!beenVisited) {
                    openHeap.push(neighbour)
                } else {
                    openHeap.rescoreElement(neighbour)
                }
            }
        }
    }

    return []
}
