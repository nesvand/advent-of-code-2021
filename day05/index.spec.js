import { readData, answerA, answerB } from './index'

const testData = readData('./test')
const inputData = readData('./input')

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: testData,
            result: 5,
        },
        {
            name: 'input data',
            data: inputData,
            result: 5145,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'test data',
            data: testData,
            result: 12,
        },
        {
            name: 'input data',
            data: inputData,
            result: 16518,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
