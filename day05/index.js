import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

function areaBounds(data) {
    return data.reduce(
        (maxes, curr) => {
            if (curr.a.x > maxes.x) maxes.x = curr.a.x
            if (curr.a.y > maxes.y) maxes.y = curr.a.y
            if (curr.b.x > maxes.x) maxes.x = curr.b.x
            if (curr.b.y > maxes.y) maxes.y = curr.b.y
            return maxes
        },
        { x: 0, y: 0 },
    )
}

function straightPoints(data) {
    const points = []
    for (let datum of data) {
        if (datum.a.x === datum.b.x && datum.a.y !== datum.b.y) {
            for (let y = Math.min(datum.a.y, datum.b.y); y <= Math.max(datum.a.y, datum.b.y); y++) {
                points.push({ x: datum.a.x, y })
            }
        } else if (datum.a.y === datum.b.y && datum.a.x !== datum.b.x) {
            for (let x = Math.min(datum.a.x, datum.b.x); x <= Math.max(datum.a.x, datum.b.x); x++) {
                points.push({ x, y: datum.a.y })
            }
        }
    }

    return points
}

function allPoints(data) {
    const points = []
    for (let datum of data) {
        if (datum.a.x === datum.b.x && datum.a.y !== datum.b.y) {
            for (let y = Math.min(datum.a.y, datum.b.y); y <= Math.max(datum.a.y, datum.b.y); y++) {
                points.push({ x: datum.a.x, y })
            }
        } else if (datum.a.y === datum.b.y && datum.a.x !== datum.b.x) {
            for (let x = Math.min(datum.a.x, datum.b.x); x <= Math.max(datum.a.x, datum.b.x); x++) {
                points.push({ x, y: datum.a.y })
            }
        } else if (Math.abs(datum.a.x - datum.b.x) === Math.abs(datum.a.y - datum.b.y)) {
            const negx = datum.a.x > datum.b.x
            const negy = datum.a.y > datum.b.y

            for (
                let x = datum.a.x, y = datum.a.y;
                negx ? x >= datum.b.x : x <= datum.b.x;
                negx ? --x : ++x, negy ? --y : ++y
            ) {
                points.push({ x, y })
            }
        }
    }

    return points
}

function enumeratePoints(points, incDiagonals) {
    const { x: width, y: height } = areaBounds(points)

    function pointToIndex(x, y) {
        return y * (width + 1) + x
    }

    const board = Array.from({ length: (width + 1) * (height + 1) }, () => 0)
    const validPoints = incDiagonals ? allPoints(points) : straightPoints(points)

    for (let point of validPoints) {
        board[pointToIndex(point.x, point.y)]++
    }

    return board
}

export function answerA(lines) {
    const data = lines.map((line) => {
        const [a, , b] = line.split(' ')
        const [ax, ay] = a.split(',')
        const [bx, by] = b.split(',')
        return {
            a: { x: parseInt(ax), y: parseInt(ay) },
            b: { x: parseInt(bx), y: parseInt(by) },
        }
    })

    const { x, y } = areaBounds(data)
    const board = enumeratePoints(data)

    return board.filter((v) => v > 1).length
}

export function answerB(lines) {
    const data = lines.map((line) => {
        const [a, , b] = line.split(' ')
        const [ax, ay] = a.split(',')
        const [bx, by] = b.split(',')
        return {
            a: { x: parseInt(ax), y: parseInt(ay) },
            b: { x: parseInt(bx), y: parseInt(by) },
        }
    })

    const { x, y } = areaBounds(data)
    const board = enumeratePoints(data, true)

    return board.filter((v) => v > 1).length
}
