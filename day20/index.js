import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

function charToNum(char) {
    return char === '#' ? 1 : 0
}

function enhanceImage(enhancer, image, border) {
    const newImage = []
    for (let y = -1; y < image.length + 1; y++) {
        const row = []
        for (let x = -1; x < image[0].length + 1; x++) {
            let pointer = 0
            for (let yy of [-1, 0, 1]) {
                for (let xx of [-1, 0, 1]) {
                    pointer *= 2
                    pointer += image?.[y + yy]?.[x + xx] ?? border
                }
            }
            row.push(enhancer[pointer])
        }
        newImage.push(row)
    }

    return {
        newImage,
        newBorder: border ? enhancer[enhancer.length - 1] : enhancer[0],
    }
}

export function answerA(lines, passes = 2) {
    const enhancer = lines[0].split``.map(charToNum)
    lines.shift()
    lines.shift()
    let baseImage = lines.map((line) => line.split``.map(charToNum))

    let border = 0
    let currentImage = baseImage
    for (let i = 0; i < passes; i++) {
        const { newImage, newBorder } = enhanceImage(enhancer, currentImage, border)
        currentImage = newImage
        border = newBorder
    }

    return currentImage.reduce((total, row) => total + row.reduce((a, b) => a + b), 0)
}

export function answerB(lines) {
    return answerA(lines, 50)
}
