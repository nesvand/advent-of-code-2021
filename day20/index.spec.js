import { readData, answerA, answerB } from './index'

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: readData('./test'),
            result: 35,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 5498,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'test data',
            data: readData('./test'),
            result: 3351,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 16014,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
