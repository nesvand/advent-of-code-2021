import { readData, answerA, answerB } from './index'

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: ['target area: x=20..30, y=-10..-5'],
            result: 45,
        },
        {
            name: 'input data',
            data: ['target area: x=201..230, y=-99..-65'],
            result: 4851,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'test data',
            data: ['target area: x=20..30, y=-10..-5'],
            result: 112,
        },
        {
            name: 'input data',
            data: ['target area: x=201..230, y=-99..-65'],
            result: 1739,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
