import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

function simulateHittingBounds(xMin, xMax, yMin, yMax) {
    return function (vx, vy) {
        let x = 0,
            y = 0
        let maxHeight = -Infinity

        while (x <= xMax && y >= yMin) {
            x += vx
            y += vy
            maxHeight = Math.max(maxHeight, y)
            vx -= Math.sign(vx)
            vy--
            if (x >= xMin && x <= xMax && y >= yMin && y <= yMax) return maxHeight
        }
    }
}

function processBounds(x1, x2, y1, y2) {
    const xMin = Math.min(x1, x2)
    const xMax = Math.max(x1, x2)
    const yMin = Math.min(y1, y2)
    const yMax = Math.max(y1, y2)

    const simulate = simulateHittingBounds(xMin, xMax, yMin, yMax)

    let maxHeight = -Infinity
    let count = 0
    for (let vx = 0; vx <= xMax; vx++) {
        for (let vy = yMin; vy < -yMin; vy++) {
            let maxSimHeight = simulate(vx, vy)
            if (maxSimHeight !== undefined) {
                count++
                maxHeight = Math.max(maxHeight, maxSimHeight)
            }
        }
    }

    return { maxHeight, count }
}

export function answerA(lines) {
    const [_, xs, ys] = /.*?x=(-?\d+..-?\d+), y=(-?\d+..-?\d+)/.exec(lines[0])
    const [x1, x2] = xs.split`..`.map(Number)
    const [y1, y2] = ys.split`..`.map(Number)

    const { maxHeight } = processBounds(x1, x2, y1, y2)
    return maxHeight
}

export function answerB(lines) {
    const [_, xs, ys] = /.*?x=(-?\d+..-?\d+), y=(-?\d+..-?\d+)/.exec(lines[0])
    const [x1, x2] = xs.split`..`.map(Number)
    const [y1, y2] = ys.split`..`.map(Number)

    const { count } = processBounds(x1, x2, y1, y2)
    return count
}
