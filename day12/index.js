import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

function dfsPartA(graph, node, visited, paths) {
    visited.push(node)
    if (node === 'end') {
        paths.push(visited.join(','))
        return
    }

    for (const neighbour of graph[node]) {
        if (/[a-z]/.test(neighbour) && visited.includes(neighbour)) continue
        dfsPartA(graph, neighbour, [...visited], paths)
    }
}

function dfsPartB(graph, node, visited, paths, visitedTwice = false) {
    visited.push(node)
    if (node === 'end') {
        paths.push(visited.join(','))
        return
    }

    for (const neighbour of graph[node]) {
        if (neighbour === 'start') continue

        if (/[a-z]/.test(neighbour) && visited.includes(neighbour)) {
            if (visitedTwice) continue
            if (visited.filter((x) => x === neighbour).length >= 2) continue
            dfsPartB(graph, neighbour, [...visited], paths, true)
        } else {
            dfsPartB(graph, neighbour, [...visited], paths, visitedTwice)
        }
    }
}

export function answerA(lines) {
    const graph = lines.reduce((g, line) => {
        const [from, to] = line.split('-')

        if (!g[from]) {
            g[from] = []
        }

        if (!g[to]) {
            g[to] = []
        }

        g[from].push(to)
        g[to].push(from)

        return g
    }, {})

    const paths = []
    dfsPartA(graph, 'start', [], paths)
    return paths.length
}

export function answerB(lines) {
    const graph = lines.reduce((g, line) => {
        const [from, to] = line.split('-')

        if (!g[from]) {
            g[from] = []
        }

        if (!g[to]) {
            g[to] = []
        }

        g[from].push(to)
        g[to].push(from)

        return g
    }, {})

    const paths = []
    dfsPartB(graph, 'start', [], paths)
    return paths.length
}
