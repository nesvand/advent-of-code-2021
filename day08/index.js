import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

export function answerA(lines) {
    const data = lines.map((line) => {
        const [signals, outputs] = line.split(' | ').map((segment) =>
            segment.split(' ').map((string) => {
                const letters = [...string]
                letters.sort()
                return letters.join('')
            }),
        )
        return { signals, outputs }
    })

    let sum = 0
    for (const line of data) {
        const matches = line.outputs.filter((val) => [2, 3, 4, 7].includes(val.length))
        sum += matches.length
    }

    return sum
}

// Checks if all of b is included in a
function includes(a, b) {
    const set = new Set([...a])
    return [...b].every((x) => set.has(x))
}

export function answerB(lines) {
    const data = lines.map((line) => {
        const [signals, outputSequence] = line.split(' | ').map((segment) =>
            segment.split(' ').map((string) => {
                const letters = [...string]
                letters.sort()
                return letters.join('')
            }),
        )
        return { signals, outputSequence }
    })

    let sum = 0
    for (const line of data) {
        const matches = {
            1: line.signals.find((x) => x.length === 2),
            4: line.signals.find((x) => x.length === 4),
            7: line.signals.find((x) => x.length === 3),
            8: line.signals.find((x) => x.length === 7),
        }

        // Characters length of 6 (6, 9, 0)
        matches[6] = line.signals.find((x) => x.length === 6 && !includes(x, matches[1]))
        matches[9] = line.signals.find((x) => x.length === 6 && !includes(x, matches[6]) && includes(x, matches[4]))
        matches[0] = line.signals.find((x) => x.length === 6 && x !== matches[6] && x !== matches[9])

        // Characters of length 5 (2, 3, 5)
        matches[3] = line.signals.find((x) => x.length === 5 && includes(x, matches[1]))
        matches[5] = line.signals.find((x) => x.length === 5 && x !== matches[3] && includes(matches[6], x))
        matches[2] = line.signals.find((x) => x.length === 5 && x !== matches[3] && x !== matches[5])

        // Inverts the matching table, providing a lookup table for the value of a given sequence
        const lookupTable = Object.fromEntries(Object.entries(matches).map((x) => x.reverse()))

        const output = Number(line.outputSequence.map((o) => lookupTable[o]).join``)

        sum += output
    }

    return sum
}
