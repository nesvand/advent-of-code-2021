import { readData, answerA, answerB } from './index'

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: readData('./test'),
            result: 26,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 519,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'test data',
            data: readData('./test'),
            result: 61229,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 1027483,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
