import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

function counts(readings) {
    return readings.reduce(
        (acc, curr) => {
            for (let i = 0; i < curr.length; i++) {
                acc[i][curr[i]]++
            }

            return acc
        },
        Array.from({ length: readings[0].length }, () => ({ 0: 0, 1: 0 })),
    )
}

function gammaEpsilon(readings) {
    return counts(readings).reduce(
        (gE, count, pos, arr) => {
            if (count['0'] > count['1']) {
                gE.epsilon = gE.epsilon | (1 << (arr.length - (pos + 1)))
            } else {
                gE.gamma = gE.gamma | (1 << (arr.length - (pos + 1)))
            }
            return gE
        },
        { gamma: 0, epsilon: 0 },
    )
}

export function answerA(lines) {
    const readings = lines.map((line) => Array.from(line))

    const { gamma, epsilon } = gammaEpsilon(readings)

    return gamma * epsilon
}

function oxygenGeneratorRating(readings) {
    let remainingReadings = [...readings]
    let pos = 0
    while (remainingReadings.length !== 1) {
        const c = counts(remainingReadings)
        remainingReadings = remainingReadings.filter((reading) => {
            if (c[pos]['0'] > c[pos]['1']) {
                return reading[pos] === '0'
            } else {
                return reading[pos] === '1'
            }
        })

        pos++
    }

    return parseInt(remainingReadings[0].join(''), 2)
}

function c02ScrubberRating(readings) {
    let remainingReadings = [...readings]
    let pos = 0
    while (remainingReadings.length !== 1) {
        const c = counts(remainingReadings)
        remainingReadings = remainingReadings.filter((reading) => {
            if (c[pos]['0'] > c[pos]['1']) {
                return reading[pos] === '1'
            } else {
                return reading[pos] === '0'
            }
        })

        pos++
    }

    return parseInt(remainingReadings[0].join(''), 2)
}

export function answerB(lines) {
    const readings = lines.map((line) => Array.from(line))

    return oxygenGeneratorRating(readings) * c02ScrubberRating(readings)
}
