import { readData, answerA, answerB } from './index'

const testData = readData('./test')
const inputData = readData('./input')

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: testData,
            result: 198,
        },
        {
            name: 'input data',
            data: inputData,
            result: 3309596,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'test data',
            data: testData,
            result: 230,
        },
        {
            name: 'input data',
            data: inputData,
            result: 2981085,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
