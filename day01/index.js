import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

export function answerA(lines) {
    return lines.map(Number).reduce((count, curr, i, arr) => {
        if (i === 0) {
            return count
        }

        return curr > arr[i - 1] ? ++count : count
    }, 0)
}

function slidingWindows(lines) {
    const data = lines.map(Number)
    const windows = []
    for (let i = 1; i < data.length - 1; i++) {
        windows.push(data[i - 1] + data[i] + data[i + 1])
    }

    return windows
}

export function answerB(lines) {
    return answerA(slidingWindows(lines))
}
