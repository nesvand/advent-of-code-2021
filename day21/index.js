import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

export function answerA(lines) {
    let [p1Position, p2Position] = lines

    let currDieVal = 1
    let p1Score = 0
    let p2Score = 0

    let rollCount = 0
    while (true) {
        rollCount += 3
        let p1DiceSum = currDieVal <= 98 ? currDieVal * 3 + 3 : currDieVal === 99 ? 200 : 103

        currDieVal = ((currDieVal + 2) % 100) + 1
        p1Position = ((p1Position + p1DiceSum - 1) % 10) + 1
        p1Score += p1Position
        if (p1Score >= 1000) break

        rollCount += 3
        let p2DiceSum = currDieVal <= 98 ? currDieVal * 3 + 3 : currDieVal === 99 ? 200 : 103
        currDieVal = ((currDieVal + 2) % 100) + 1
        p2Position = ((p2Position + p2DiceSum - 1) % 10) + 1
        p2Score += p2Position
        if (p2Score >= 1000) break
    }

    return rollCount * Math.min(p1Score, p2Score)
}

export function answerB(lines) {
    let [p1Position, p2Position] = lines

    const combinations = []
    for (let i = 1; i <= 3; i++) {
        for (let j = 1; j <= 3; j++) {
            for (let k = 1; k <= 3; k++) {
                combinations.push([i, j, k])
            }
        }
    }

    const combinationCounts = new Map()
    for (const c of combinations) {
        const sum = c[0] + c[1] + c[2]
        combinationCounts.set(sum, (combinationCounts.get(sum) ?? 0) + 1)
    }

    const wins = new Map()

    function play(player1, player2, p1Score, p2Score) {
        if (p2Score >= 21) return [0, 1]

        const key = [player1, player2, p1Score, p2Score].toString()
        const cached = wins.get(key)
        if (cached !== undefined) return cached

        let res = [0, 0]

        for (const [sum, count] of combinationCounts.entries()) {
            const nextPlayer1 = ((player1 + sum - 1) % 10) + 1
            let nextPlayer1Score = p1Score + nextPlayer1
            let [nextCount1, nextCount2] = play(player2, nextPlayer1, p2Score, nextPlayer1Score)
            res[0] += nextCount2 * count
            res[1] += nextCount1 * count
        }

        wins.set(key, res)
        return res
    }

    const counts = play(p1Position, p2Position, 0, 0)

    return Math.max(...counts)
}
