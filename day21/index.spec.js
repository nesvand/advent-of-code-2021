import { readData, answerA, answerB } from './index'

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: [4, 8],
            result: 739785,
        },
        {
            name: 'input data',
            data: [2, 8],
            result: 1196172,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'test data',
            data: [4, 8],
            result: 444356092776315,
        },
        {
            name: 'input data',
            data: [2, 8],
            result: 106768284484217,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
