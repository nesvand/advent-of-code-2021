import { readData, answerA, answerB } from './index'

const testData = readData('./test')
const inputData = readData('./input')

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: testData,
            result: 150,
        },
        {
            name: 'input data',
            data: inputData,
            result: 1690020,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'test data',
            data: testData,
            result: 900,
        },
        {
            name: 'input data',
            data: inputData,
            result: 1408487760,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
