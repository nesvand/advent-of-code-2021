import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

export function answerA(lines) {
    const instructions = lines.map((instruction) => {
        const [direction, amount] = instruction.split(' ')
        return [direction, Number(amount)]
    })

    const { horizontal, depth } = instructions.reduce(
        (pos, instruction) => {
            switch (instruction[0]) {
                case 'forward':
                    pos.horizontal += instruction[1]
                    break
                case 'down':
                    pos.depth += instruction[1]
                    break
                case 'up':
                    pos.depth -= instruction[1]
                    break
            }

            return pos
        },
        { horizontal: 0, depth: 0 },
    )

    return horizontal * depth
}

export function answerB(lines) {
    const instructions = lines.map((instruction) => {
        const [direction, amount] = instruction.split(' ')
        return [direction, Number(amount)]
    })

    const { horizontal, depth } = instructions.reduce(
        (pos, [direction, x]) => {
            switch (direction) {
                case 'forward':
                    pos.horizontal += x
                    pos.depth += x * pos.aim
                    break
                case 'down':
                    pos.aim += x
                    break
                case 'up':
                    pos.aim -= x
                    break
                default:
                    break
            }

            return pos
        },
        { aim: 0, depth: 0, horizontal: 0 },
    )

    return horizontal * depth
}
