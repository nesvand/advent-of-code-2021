import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

export function answerA(lines) {
    const initialCoords = lines
        .filter((line) => line.includes`,`)
        .map((line) => {
            const [x, y] = line.split`,`
            return { x: Number(x), y: Number(y) }
        })
    const instructions = lines
        .filter((line) => line.includes`fold along`)
        .map((line) => {
            const [axis, val] = [...line].slice(11).join``.split`=`
            return { axis, val: Number(val) }
        })
    let width = initialCoords.map(({ x }) => x).reduce((a, b) => Math.max(a, b)) + 1
    let height = initialCoords.map(({ y }) => y).reduce((a, b) => Math.max(a, b)) + 1

    let paper = Array(height)
        .fill(0)
        .map((_, y) =>
            Array(width)
                .fill(0)
                .map((_, x) => {
                    return initialCoords.some((coord) => coord.x === x && coord.y === y) ? 1 : 0
                }),
        )

    const instruction = instructions[0]
    switch (instruction.axis) {
        case 'y': {
            height = instruction.val
            const top = paper.slice(0, height)
            const bottomReversed = paper.slice(height + 1).reverse()

            const newPaper = Array(height)
                .fill(0)
                .map(() => Array(width).fill(0))

            for (let y = 0; y < height; y++) {
                for (let x = 0; x < width; x++) {
                    newPaper[y][x] = (top?.[y]?.[x] ?? 0) || (bottomReversed?.[y]?.[x] ?? 0)
                }
            }

            paper = newPaper

            // for (const line of paper) {
            //     console.log(line.map((x) => x ? '#' : '.').join``)
            // }
            break
        }
        case 'x': {
            width = instruction.val
            const [left, rightReversed] = paper.reduce(
                (sides, line) => {
                    sides[0].push(line.slice(0, width))
                    sides[1].push(line.slice(width + 1).reverse())
                    return sides
                },
                [[], []],
            )

            const newPaper = Array(height)
                .fill(0)
                .map(() => Array(width).fill(0))

            for (let y = 0; y < height; y++) {
                for (let x = 0; x < width; x++) {
                    newPaper[y][x] = (left?.[y]?.[x] ?? 0) || (rightReversed?.[y]?.[x] ?? 0)
                }
            }

            paper = newPaper

            // for (const line of paper) {
            //     console.log(line.map((x) => x ? '#' : '.').join``)
            // }
            break
        }
        default:
    }

    let sum = 0
    for (const line of paper) {
        sum += line.reduce((a, b) => a + b)
    }

    return sum
}

export function answerB(lines) {
    const initialCoords = lines
        .filter((line) => line.includes`,`)
        .map((line) => {
            const [x, y] = line.split`,`
            return { x: Number(x), y: Number(y) }
        })
    const instructions = lines
        .filter((line) => line.includes`fold along`)
        .map((line) => {
            const [axis, val] = [...line].slice(11).join``.split`=`
            return { axis, val: Number(val) }
        })
    let width = initialCoords.map(({ x }) => x).reduce((a, b) => Math.max(a, b)) + 1
    let height = initialCoords.map(({ y }) => y).reduce((a, b) => Math.max(a, b)) + 1

    let paper = Array(height)
        .fill(0)
        .map((_, y) =>
            Array(width)
                .fill(0)
                .map((_, x) => {
                    return initialCoords.some((coord) => coord.x === x && coord.y === y) ? 1 : 0
                }),
        )

    for (const instruction of instructions) {
        switch (instruction.axis) {
            case 'y': {
                const top = paper.slice(0, instruction.val).reverse()
                const bottomReversed = paper.slice(instruction.val + 1)

                height = Math.max(top.length, bottomReversed.length)

                const newPaper = Array(height)
                    .fill(0)
                    .map(() => Array(width).fill(0))

                for (let y = 0; y < height; y++) {
                    for (let x = 0; x < width; x++) {
                        newPaper[y][x] = (top?.[y]?.[x] ?? 0) || (bottomReversed?.[y]?.[x] ?? 0)
                    }
                }

                paper = newPaper.reverse()
                break
            }
            case 'x': {
                const [left, rightReversed] = paper.reduce(
                    (sides, line) => {
                        sides[0].push(line.slice(0, instruction.val).reverse())
                        sides[1].push(line.slice(instruction.val + 1))
                        return sides
                    },
                    [[], []],
                )

                width = Math.max(left[0].length, rightReversed[0].length)

                const newPaper = Array(height)
                    .fill(0)
                    .map(() => Array(width).fill(0))

                for (let y = 0; y < height; y++) {
                    for (let x = 0; x < width; x++) {
                        newPaper[y][x] = (left?.[y]?.[x] ?? 0) || (rightReversed?.[y]?.[x] ?? 0)
                    }
                }

                paper = newPaper.map((line) => line.reverse())

                break
            }
            default:
        }
    }

    let output = ``
    for (const line of paper) {
        output += line.map((x) => (x ? '#' : '.')).join`` + '\n'
    }

    return output
}
