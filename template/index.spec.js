import { readData, answerA, answerB } from './index'

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: readData('./test'),
            result: undefined,
        },
        // {
        //     name: 'input data',
        //     data: readData('./input'),
        //     result: undefined,
        // },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        // {
        //     name: 'test data',
        //     data: readData('./test'),
        //     result: undefined,
        // },
        // {
        //     name: 'input data',
        //     data: readData('./input'),
        //     result: undefined,
        // },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
