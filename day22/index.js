import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

class Cube {
    constructor(cube) {
        this.state = cube.state
        this.x1 = cube.x1
        this.x2 = cube.x2
        this.y1 = cube.y1
        this.y2 = cube.y2
        this.z1 = cube.z1
        this.z2 = cube.z2
    }
    contains(cube) {
        return (
            this.x1 <= cube.x1 &&
            this.x2 >= cube.x2 &&
            this.y1 <= cube.y1 &&
            this.y2 >= cube.y2 &&
            this.z1 <= cube.z1 &&
            this.z2 >= cube.z2
        )
    }
    intersects(cube) {
        return (
            this.x1 <= cube.x2 &&
            this.x2 >= cube.x1 &&
            this.y1 <= cube.y2 &&
            this.y2 >= cube.y1 &&
            this.z1 <= cube.z2 &&
            this.z2 >= cube.z1
        )
    }
    volume() {
        return (this.x2 - this.x1) * (this.y2 - this.y1) * (this.z2 - this.z1)
    }
    naiveSplit(cube) {
        if (this.contains(cube)) return []
        if (!this.intersects(cube)) return [this]

        let xs = [this.x1, ...[cube.x1, cube.x2].filter((x) => this.x1 < x && x < this.x2), this.x2]
        let ys = [this.y1, ...[cube.y1, cube.y2].filter((y) => this.y1 < y && y < this.y2), this.y2]
        let zs = [this.z1, ...[cube.z1, cube.z2].filter((z) => this.z1 < z && z < this.z2), this.z2]

        const splits = []
        for (let i = 0; i < xs.length - 1; i++) {
            for (let j = 0; j < ys.length - 1; j++) {
                for (let k = 0; k < zs.length - 1; k++) {
                    splits.push(
                        new Cube({
                            x1: xs[i],
                            x2: xs[i + 1],
                            y1: ys[j],
                            y2: ys[j + 1],
                            z1: zs[k],
                            z2: zs[k + 1],
                        }),
                    )
                }
            }
        }

        return splits.filter((c) => !cube.contains(c))
    }
}

function parseSteps(lines, filter = true) {
    const steps = []
    for (let line of lines) {
        let [_, state, x1, x2, y1, y2, z1, z2] =
            /(on|off) x=(-?\d+)..(-?\d+),y=(-?\d+)..(-?\d+),z=(-?\d+)..(-?\d+)/.exec(line)
        steps.push(
            new Cube({
                state: state === 'on' ? 1 : 0,
                x1: BigInt(x1),
                x2: BigInt(x2) + 1n,
                y1: BigInt(y1),
                y2: BigInt(y2) + 1n,
                z1: BigInt(z1),
                z2: BigInt(z2) + 1n,
            }),
        )
    }

    return filter
        ? steps.filter(({ x1, x2, y1, y2, z1, z2 }) =>
              x2 < -50 || x1 > 50 || y2 < -50 || y1 > 50 || z2 < -50 || z1 > 50 ? false : true,
          )
        : steps
}

export function answerA(lines, filter = true) {
    const steps = parseSteps(lines, filter)
    let cubes = []
    for (let step of steps) {
        cubes = cubes.flatMap((cube) => cube.naiveSplit(step))

        if (step.state) {
            cubes.push(step)
        }
    }

    return cubes.map((c) => c.volume()).reduce((a, b) => a + b, 0n)
}

export function answerB(lines) {
    return answerA(lines, false)
}
