import { readData, answerA, answerB } from './index'

describe('answerA', () => {
    const tests = [
        {
            name: 'test data',
            data: readData('./test'),
            result: 4512,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 12796,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'test data',
            data: readData('./test'),
            result: 1924,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 18063,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
