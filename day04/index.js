import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().split('\n')
}

function markNumber(num, board) {
    for (const entry of board) {
        if (entry.num === num) {
            entry.marked = true
            return true
        }
    }

    return false
}

function coordAsIndex(x, y) {
    return 5 * y + x
}

function hasBingo(board) {
    // Horizontal lines
    for (let y = 0; y < 5; y++) {
        let bingo = true
        for (let x = 0; x < 5; x++) {
            bingo = bingo && board[coordAsIndex(x, y)].marked
        }
        if (bingo) {
            return true
        }
    }

    // Vertical lines
    for (let x = 0; x < 5; x++) {
        let bingo = true
        for (let y = 0; y < 5; y++) {
            bingo = bingo && board[coordAsIndex(x, y)].marked
        }
        if (bingo) {
            return true
        }
    }

    return false
}

function playUntilWin({ inputs, boards }) {
    const bingos = []
    while (!bingos.length && inputs.length) {
        const input = parseInt(inputs.shift())

        for (let board of boards) {
            if (markNumber(input, board)) {
                const bingo = hasBingo(board)
                if (bingo) bingos.push({ input, board })
            }
        }
    }

    return bingos
}

function score({ input, board }) {
    return input * board.reduce((sum, curr) => sum + (curr.marked ? 0 : curr.num), 0)
}

export function answerA(lines) {
    const inputs = lines.shift().split(',')
    lines.shift() // throwing out empty line
    const boards = []

    while (lines.length) {
        const board = []
        for (let i = 0; i < 5; i++) {
            board.push(
                ...lines
                    .shift()
                    .split(' ')
                    .filter(Boolean)
                    .map((num) => ({ num: parseInt(num, 10), marked: false })),
            )
        }
        lines.shift() // throwing out empty line
        boards.push(board)
    }

    const bingos = playUntilWin({ inputs, boards })
    return score({ input: bingos[0].input, board: bingos[0].board })
}

export function answerB(lines) {
    const inputs = lines.shift().split(',')
    lines.shift() // throwing out empty line
    const boards = []

    while (lines.length) {
        const board = []
        for (let i = 0; i < 5; i++) {
            board.push(
                ...lines
                    .shift()
                    .split(' ')
                    .filter(Boolean)
                    .map((num) => ({ num: parseInt(num, 10), marked: false })),
            )
        }
        lines.shift() // throwing out empty line
        boards.push(board)
    }

    let lastInput, lastBoard
    while (boards.length) {
        const bingos = playUntilWin({ inputs, boards })
        lastInput = bingos[bingos.length - 1].input
        lastBoard = bingos[bingos.length - 1].board

        for (let bingo of bingos) {
            boards.splice(boards.indexOf(bingo.board), 1)
        }
    }

    return score({ input: lastInput, board: lastBoard })
}
