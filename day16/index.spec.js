import { readData, answerA, answerB } from './index'

describe('answerA', () => {
    const tests = [
        {
            name: '8A004A801A8002F478',
            data: ['8A004A801A8002F478'],
            result: 16,
        },
        {
            name: '620080001611562C8802118E34',
            data: ['620080001611562C8802118E34'],
            result: 12,
        },
        {
            name: 'C0015000016115A2E0802F182340',
            data: ['C0015000016115A2E0802F182340'],
            result: 23,
        },
        {
            name: 'A0016C880162017C3686B18A3D4780',
            data: ['A0016C880162017C3686B18A3D4780'],
            result: 31,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 945,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerA(t.data)).toBe(t.result)
        })
    }
})

describe('answerB', () => {
    const tests = [
        {
            name: 'Sum : C200B40A82',
            data: ['C200B40A82'],
            result: 3,
        },
        {
            name: 'Product : 04005AC33890',
            data: ['04005AC33890'],
            result: 54,
        },
        {
            name: 'Min : 880086C3E88112',
            data: ['880086C3E88112'],
            result: 7,
        },
        {
            name: 'Max : CE00C43D881120',
            data: ['CE00C43D881120'],
            result: 9,
        },
        {
            name: 'GT : D8005AC2A8F0',
            data: ['D8005AC2A8F0'],
            result: 1,
        },
        {
            name: 'LT : F600BC2D8F',
            data: ['F600BC2D8F'],
            result: 0,
        },
        {
            name: 'EQ : 9C005AC2F8F0',
            data: ['9C005AC2F8F0'],
            result: 0,
        },
        {
            name: '1 + 3 = 2 * 2 : 9C0141080250320F1802104A08',
            data: ['9C0141080250320F1802104A08'],
            result: 1,
        },
        {
            name: 'input data',
            data: readData('./input'),
            result: 10637009915279,
        },
    ]

    for (const t of tests) {
        test(t.name, () => {
            expect(answerB(t.data)).toBe(t.result)
        })
    }
})
