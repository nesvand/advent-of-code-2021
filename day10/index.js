import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

const pairs = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>',
}

const corruptedPoints = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137,
}

const completionPoints = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4,
}

const openers = Object.keys(pairs)
const closers = Object.values(pairs)

function parseChunk(chunk) {
    const stack = []
    for (let i = 0; i < chunk.length; i++) {
        const top = stack.length ? stack[stack.length - 1] : null

        if (!top && i === 0) {
            const val = chunk[i]

            if (openers.includes(val)) {
                stack.push(val)
                continue
            }

            return { corrupted: true, stoppedOn: val }
        }

        if (top) {
            const val = chunk[i]
            if (closers.includes(val)) {
                if (pairs[top] !== val) {
                    return { corrupted: true, stoppedOn: val }
                }

                stack.pop()
            } else {
                stack.push(val)
            }
        } else {
            const val = chunk[i]
            if (openers.includes(val)) {
                stack.push(chunk[i])
            } else {
                return { corrupted: true, stoppedOn: chunk[i] }
            }
        }
    }

    return { corrupted: false, stoppedOn: null, incomplete: stack }
}

function scoreCompletion(chunk) {
    const completion = []
    for (let i = chunk.incomplete.length - 1; i >= 0; i--) {
        completion.push(closers[openers.indexOf(chunk.incomplete[i])])
    }

    return completion.reduce((total, val) => total * 5 + completionPoints[val], 0)
}

export function answerA(lines) {
    const parsedChunks = lines.map((chunk) => chunk.split('')).map(parseChunk)

    return parsedChunks
        .filter((chunk) => chunk.corrupted)
        .reduce((sum, chunk) => sum + corruptedPoints[chunk.stoppedOn], 0)
}

export function answerB(lines) {
    const parsedChunks = lines.map((chunk) => chunk.split('')).map(parseChunk)

    const completionScores = parsedChunks
        .filter((chunk) => !chunk.corrupted)
        .map(scoreCompletion)
        .sort((a, b) => a - b)

    return completionScores[Math.floor(completionScores.length / 2)]
}
