import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

function parseTree(valOrPair, parent = null) {
    if (typeof valOrPair === 'number') {
        return { type: 'leaf', val: valOrPair, parent }
    }

    const node = { type: 'node', parent }
    const [left, right] = valOrPair
    node.left = parseTree(left, node)
    node.right = parseTree(right, node)

    return node
}

function flat(nodeOrLeaf) {
    if (nodeOrLeaf.type === 'leaf') {
        return nodeOrLeaf.val
    }

    return [flat(nodeOrLeaf.left), flat(nodeOrLeaf.right)]
}

function join(left, right) {
    const tree = { type: 'node', left, right }

    left.parent = tree
    right.parent = tree

    return tree
}

function listLeaves(nodeOrLeaf) {
    if (nodeOrLeaf.type === 'leaf') {
        return [nodeOrLeaf]
    } else {
        return listLeaves(nodeOrLeaf.left).concat(listLeaves(nodeOrLeaf.right))
    }
}

function explode(nodeOrLeaf, depth = 0) {
    if (
        nodeOrLeaf.type === 'node' &&
        nodeOrLeaf.left.type === 'leaf' &&
        nodeOrLeaf.right.type === 'leaf' &&
        depth >= 4
    ) {
        const zero = parseTree(0, nodeOrLeaf.parent)

        if (nodeOrLeaf.parent.left === nodeOrLeaf) {
            nodeOrLeaf.parent.left = zero
        } else if (nodeOrLeaf.parent.right === nodeOrLeaf) {
            nodeOrLeaf.parent.right = zero
        }

        return [zero, nodeOrLeaf.left.val, nodeOrLeaf.right.val]
    } else if (nodeOrLeaf.type === 'node') {
        const left = explode(nodeOrLeaf.left, depth + 1)
        if (left !== null) return left

        const right = explode(nodeOrLeaf.right, depth + 1)
        if (right !== null) return right
    }

    return null
}

function split(nodeOrLeaf) {
    if (nodeOrLeaf.type === 'leaf' && nodeOrLeaf.val >= 10) {
        const tree = parseTree([Math.floor(nodeOrLeaf.val / 2), Math.ceil(nodeOrLeaf.val / 2)], nodeOrLeaf.parent)
        if (nodeOrLeaf.parent.left === nodeOrLeaf) {
            nodeOrLeaf.parent.left = tree
        } else if (nodeOrLeaf.parent.right === nodeOrLeaf) {
            nodeOrLeaf.parent.right = tree
        } else {
            throw Exception('Unexpected split')
        }

        return true
    } else if (nodeOrLeaf.type === 'node') {
        if (split(nodeOrLeaf.left)) return true
        if (split(nodeOrLeaf.right)) return true
    }

    return false
}

function calculate(tree) {
    let changed
    do {
        changed = false

        const exploded = explode(tree)
        if (exploded !== null) {
            const [zero, left, right] = exploded
            const pieces = listLeaves(tree)
            const i = pieces.indexOf(zero)
            if (i < 0) throw Exception('unexpected tree')
            if (i > 0) pieces[i - 1].val += left
            if (i < pieces.length - 1) pieces[i + 1].val += right
            changed = true
        } else {
            changed = split(tree)
        }
    } while (changed)

    return tree
}

function magnitude(tree) {
    if (tree.type === 'leaf') {
        return tree.val
    }

    return 3 * magnitude(tree.left) + 2 * magnitude(tree.right)
}

function* combinations(r) {
    for (let i = 0; i < r.length; i++)
        for (let j = i + 1; j < r.length; j++) {
            if (i === j) continue
            yield [r[i], r[j]]
            yield [r[j], r[i]]
        }
}

export function answerA(lines) {
    lines = lines.map((line) => JSON.parse(line))

    let tree = parseTree(lines[0])
    for (let i = 1; i < lines.length; i++) {
        tree = calculate(join(tree, parseTree(lines[i])))
    }

    return magnitude(tree)
}

export function answerB(lines) {
    lines = lines.map((line) => JSON.parse(line))
    let max = null
    for (let [a, b] of combinations(lines)) {
        const mag = magnitude(calculate(join(parseTree(a), parseTree(b))))
        if (max === null || max < mag) {
            max = mag
        }
    }

    return max
}
