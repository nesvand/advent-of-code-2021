import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

function increaseEnergy(x, y, explodedSet, grid) {
    if (typeof grid[y] === 'undefined') return 0
    if (typeof grid[y][x] === 'undefined') return 0

    const key = `${x}:${y}`
    if (explodedSet.has(key)) return 0

    let flashes = 0
    grid[y][x]++
    if (grid[y][x] > 9) {
        grid[y][x] = 0
        explodedSet.add(key)
        flashes++

        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                if (i === 0 && j === 0) continue
                flashes += increaseEnergy(x + i, y + j, explodedSet, grid)
            }
        }
    }

    return flashes
}

export function answerA(lines, steps = 100) {
    const octopiGrid = lines.map((line) => [...line].map(Number))

    let flashes = 0
    let counter = 1
    while (counter <= steps) {
        counter++

        const explodedSet = new Set()

        for (let y = 0; y < octopiGrid.length; y++) {
            const row = octopiGrid[y]
            for (let x = 0; x < row.length; x++) {
                flashes += increaseEnergy(x, y, explodedSet, octopiGrid)
            }
        }
    }

    return flashes
}

export function answerB(lines) {
    const octopiGrid = lines.map((line) => [...line].map(Number))

    let firstSyncFlash = 0
    let counter = 0
    while (!firstSyncFlash) {
        counter++

        const explodedSet = new Set()

        for (let y = 0; y < octopiGrid.length; y++) {
            const row = octopiGrid[y]
            for (let x = 0; x < row.length; x++) {
                increaseEnergy(x, y, explodedSet, octopiGrid)
            }
        }

        if (firstSyncFlash === 0 && explodedSet.size === octopiGrid.length * octopiGrid[0].length)
            firstSyncFlash = counter
    }

    return firstSyncFlash
}
