import { readFileSync } from 'fs'
import { resolve } from 'path'

export function readData(filepath) {
    return readFileSync(resolve(__dirname, filepath), 'utf8').toString().trim().split('\n')
}

function templatePairs(template) {
    const p = []
    for (let i = 0; i < template.length - 1; i++) {
        p.push(template[i] + template[i + 1])
    }

    return p
}

export function answerA(lines, steps = 10) {
    const initialTemplate = [...lines.splice(0, 1)[0]]
    lines.shift()

    const insertionRules = lines
        .map((line) => line.split` -> `)
        .reduce((rules, [pair, letter]) => ((rules[pair] = letter), rules), {})

    let letterCounts = initialTemplate.reduce(
        (counts, letter) => (counts[letter] ? counts[letter]++ : (counts[letter] = 1), counts),
        {},
    )

    let pairCounts = templatePairs(initialTemplate).reduce(
        (counts, pair) => (counts[pair] ? counts[pair]++ : (counts[pair] = 1), counts),
        {},
    )

    const expandedRules = Object.entries(insertionRules).reduce(
        (expansions, [pair, letter]) => (
            (expansions[pair] = [pair.split``[0] + letter, letter + pair.split``[1]]), expansions
        ),
        {},
    )

    let counter = 0
    while (counter < steps) {
        const updatedPairCounts = {}

        for (const [pair, count] of Object.entries(pairCounts)) {
            const [left, right] = expandedRules[pair] ?? []
            if (left && right) {
                updatedPairCounts[left] ? (updatedPairCounts[left] += count) : (updatedPairCounts[left] = count)
                updatedPairCounts[right] ? (updatedPairCounts[right] += count) : (updatedPairCounts[right] = count)

                const letter = insertionRules[pair]
                letterCounts[letter] ? (letterCounts[letter] += count) : (letterCounts[letter] = count)
            }
        }

        pairCounts = updatedPairCounts
        counter++
    }

    const orderedCounts = Object.values(letterCounts).sort((a, b) => b - a)

    return orderedCounts[0] - orderedCounts[orderedCounts.length - 1]
}

export function answerB(lines, steps = 40) {
    return answerA(lines, steps)
}
